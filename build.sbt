name := "ttr-ui"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-swing" % "1.0.1",
  "com.fazecast" % "jSerialComm" % "1.3.11",
  "org.scalactic" %% "scalactic" % "2.2.6"
)

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",
  "org.mockito" % "mockito-all" % "1.9.5" % "test"
)

mainClass in (Compile, run) := Some("TtrConsoleApp")

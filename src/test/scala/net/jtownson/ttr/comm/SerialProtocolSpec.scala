package net.jtownson.ttr.comm

import org.scalatest.{FlatSpec, Matchers}

class SerialProtocolSpec extends FlatSpec with Matchers {

  val serialUtil: SerialProtocol = new SerialProtocol("port")

  "comm listing" should "contain something" in {
    val portNames: Seq[String] = serialUtil.availableSerialPorts()
    portNames should not be empty
  }
}

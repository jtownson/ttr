package net.jtownson.ttr.comm

import org.scalatest.{FlatSpec, Matchers}

import org.scalatest.prop.TableDrivenPropertyChecks._

class MachineStateSpec extends FlatSpec with Matchers {

  val cases = Table(
    ("state", "isx", "isy", "iv", "id", "it"),
    (MachineState(trajectory =  0,  direction =  0,  speed = 0,   topspinBackspin =  0,  sidespin = 0),   50,  50,  0,   50,  50),
    (MachineState(trajectory = -50, direction = -50, speed = 0,   topspinBackspin = -50, sidespin = -50), 0,   0,   0,   0,   0),
    (MachineState(trajectory =  50, direction =  50, speed = 100, topspinBackspin =  50, sidespin =  50), 100, 100, 100, 100, 100)
  )

  "MachineState" should "generate correct arduino packet values" in {
    forAll(cases) { (state, isx, isy, iv, id, it) =>
      state.isx should equal (isx)
      state.isy should equal (isy)
      state.iv  should equal (iv)
      state.id  should equal (id)
      state.it  should equal (it)
    }
  }
}

package net.jtownson.ttr.comm

import org.mockito.Mockito
import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}
import org.mockito.Mockito.verify

class MachineStateCommsSpec extends FlatSpec with Matchers with MockitoSugar {

  val serialUtil = mock[SerialProtocol]

  val machineStateComm = new MachineStateComms(serialUtil)

  "writeMachineState" should "write fields in the correct order" in {
    // given
    val machineState = MachineState(trajectory = 10, direction = -10, speed = 30, topspinBackspin = 25, sidespin = -25)

    // when
    machineStateComm.write(machineState)
    Thread.sleep(150)

    // then
    verify(serialUtil).write("H025,075,030,040,060\n")
  }

  "writeMachineState" should "not transfer unmodified values" in {
    val machineState = MachineState(trajectory = 10, direction = -10, speed = 30, topspinBackspin = 25, sidespin = -25)

    machineStateComm.write(machineState)
    machineStateComm.write(machineState)
    Thread.sleep(150)

    verify(serialUtil, Mockito.times(1)).write("H025,075,030,040,060\n")
  }
}

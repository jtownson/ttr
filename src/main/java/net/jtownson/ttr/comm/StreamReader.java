package net.jtownson.ttr.comm;

import java.io.IOException;
import java.io.InputStream;

public class StreamReader implements Runnable {

    private InputStream in;

    public StreamReader(InputStream in) {
        this.in = in;
    }

    public void run() {
        byte[] buffer = new byte[1024];
        int len;
        try {
            while ((len = this.in.read(buffer)) > -1) {
                System.out.print(new String(buffer, 0, len));
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}

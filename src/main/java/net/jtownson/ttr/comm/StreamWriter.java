package net.jtownson.ttr.comm;

import java.io.IOException;
import java.io.OutputStream;

public class StreamWriter implements Runnable {

    private OutputStream out;

    public StreamWriter(OutputStream out) {
        this.out = out;
    }

    public void run() {
        try {
            int c;
            while ((c = System.in.read()) > -1) {
                this.out.write(c);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

}

package net.jtownson.ttr.ui;

import javax.swing.*;

public class JVerticalLabel extends JLabel {

    public JVerticalLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
        setUI(new JVerticalLabelUI(false));
    }

    public JVerticalLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
        setUI(new JVerticalLabelUI(false));
    }

    public JVerticalLabel(String text) {
        super(text);
        setUI(new JVerticalLabelUI(false));
    }

    public JVerticalLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
        setUI(new JVerticalLabelUI(false));
    }

    public JVerticalLabel(Icon image) {
        super(image);
        setUI(new JVerticalLabelUI(false));
    }

    public JVerticalLabel() {
        setUI(new JVerticalLabelUI(false));
    }
}

import net.jtownson.ttr.comm.SerialProtocol

object TtrTestApp {
  def main(args: Array[String]) {
    val serialUtil = new SerialProtocol("cu.wchusbserial1420")
    println(serialUtil.write("\r"))
    println(serialUtil.write("50,50,100"))
  }
}

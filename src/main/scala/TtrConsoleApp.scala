import net.jtownson.ttr.comm.SerialProtocol

object TtrConsoleApp {

  def main(args: Array[String]): Unit = {
    new SerialProtocol("cu.wchusbserial1410").connectToStdInOut()
  }
}

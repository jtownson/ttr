package net.jtownson.ttr.comm


class StdioProtocol extends CommsProtocol {
  override def write(message: String): Int = {
    print(message)
    message.length
  }
}

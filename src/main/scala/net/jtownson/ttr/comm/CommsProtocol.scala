package net.jtownson.ttr.comm


trait CommsProtocol {
  def write(message: String): Int
}

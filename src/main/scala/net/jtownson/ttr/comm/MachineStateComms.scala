package net.jtownson.ttr.comm

import java.util.{Timer, TimerTask}
import java.util.concurrent.atomic.AtomicReference

class MachineStateComms(commsProtocol: CommsProtocol) {

  val lastState = new AtomicReference[MachineState]
  val nextState = new AtomicReference[MachineState]

  lazy val timer: Timer = {
    val writeSerial = new TimerTask() {
      override def run(): Unit = writeIfChanged()
    }
    val timer = new Timer(true)
    timer.scheduleAtFixedRate(writeSerial, 0, 50)
    timer
  }

  def write(machineState: MachineState): Unit = {
    nextState.set(machineState)
    timer
  }

  private def writeIfChanged(): Unit = {
    if (lastState.get() != nextState.get()) {

      syncLastAndNext()

      writeUnconditional(lastState.get())
    }
  }

  def syncLastAndNext(): Unit = lastState.set(nextState.get())

  def writeUnconditional(machineState: MachineState): Unit = commsProtocol.write(stringify(machineState))

  def stringify(machineState: MachineState): String = {
      f"H${machineState.isx}%03d," +
      f"${machineState.isy}%03d," +
      f"${machineState.iv}%03d," +
      f"${machineState.id}%03d," +
      f"${machineState.it}%03d\n"
  }
}

package net.jtownson.ttr.comm

case class MachineState(trajectory: Int = 0,
                        direction: Int = 0,
                        speed: Int = 0,
                        topspinBackspin: Int = 0,
                        sidespin: Int = 0) {

  def isx: Int = scale(sidespin, -50, 50, 0, 100)
  def isy: Int = scale(topspinBackspin, -50, 50, 0, 100)
  def iv:  Int = scale(speed, 0, 100, 0, 100)
  def id:  Int = scale(direction, -50, 50, 0, 100)
  def it:  Int = scale(trajectory, -50, 50, 0, 100)

  def scale(x: Int, xMin: Int, xMax: Int, yMin: Int, yMax: Int): Int = 
    (x - xMin)*(yMax-yMin)/(xMax-xMin) + yMin
}

package net.jtownson.ttr.comm

import java.io.{InputStream, OutputStream}
import java.nio.charset.StandardCharsets.UTF_8

import com.fazecast.jSerialComm.SerialPort

class SerialProtocol(portName: String) extends CommsProtocol {

  val serialPort = connect(portName)

  connectToStdIn()

  def write(message: String): Int = {
    val bytes: Array[Byte] = message.getBytes(UTF_8)
    serialPort.writeBytes(bytes, bytes.length)
  }

  def availableSerialPorts(): Seq[String] = {
    SerialPort.getCommPorts.map(port => port.getSystemPortName)
  }

  def connect(portName: String): SerialPort = {
    val serialPort: SerialPort = SerialPort.getCommPort(portName)
    serialPort.openPort()
    serialPort
  }

  def connectToStdIn(): Unit = {
    val in: InputStream = serialPort.getInputStream
    new Thread(new StreamReader(in)).start()
  }

  def connectToStdInOut(): Unit = {
    val in: InputStream = serialPort.getInputStream
    val out:OutputStream = serialPort.getOutputStream

    new Thread(new StreamReader(in)).start()
    new Thread(new StreamWriter(out)).start()
  }
}

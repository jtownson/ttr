package net.jtownson.ttr.comm

object Lift {
  def option[A,B](f: A => B): Option[A] => Option[B] = _ map f
}

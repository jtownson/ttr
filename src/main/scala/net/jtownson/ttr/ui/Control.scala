package net.jtownson.ttr.ui

import net.jtownson.ttr.comm.{MachineState, MachineStateComms, StdioProtocol}

class Control(comms: MachineStateComms) {

  var machineState: MachineState = MachineState()

  def trajectory(value: Int): Unit = {
    machineState = machineState.copy(trajectory = value*10)
    comms.write(machineState)
  }

  def direction(value: Int): Unit = {
    machineState = machineState.copy(direction = value*10)
    comms.write(machineState)
  }

  def speed(value: Int): Unit = {
    machineState = machineState.copy(speed = value*10)
    comms.write(machineState)
  }

  def topspinBackspin(value: Int): Unit = {
    machineState = machineState.copy(topspinBackspin = value*10)
    comms.write(machineState)
  }

  def sidespin(value: Int): Unit = {
    machineState = machineState.copy(sidespin = value*10)
    comms.write(machineState)
  }
}

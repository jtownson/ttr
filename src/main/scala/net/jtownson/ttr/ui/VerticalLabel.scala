package net.jtownson.ttr.ui

import javax.swing.{Icon, JLabel}

import scala.swing.Swing.EmptyIcon
import scala.swing.{Alignment, Label}

class VerticalLabel(text0: String, icon0: Icon, align: Alignment.Value) extends Label {

  override lazy val peer: JLabel =
    new JVerticalLabel(text0, icon0, align.id)


  def this() = this("", EmptyIcon, Alignment.Center)
  def this(s: String) = this(s, EmptyIcon, Alignment.Center)
}

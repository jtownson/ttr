package net.jtownson.ttr.ui

import java.awt.{Color, Dimension}
import javax.swing.border.{EmptyBorder, Border, LineBorder}

import scala.swing.GridBagPanel.Anchor.{North, South}
import scala.swing.GridBagPanel.{Anchor, Fill}
import scala.swing.GridBagPanel.Fill.{None, Vertical, Horizontal}
import scala.swing._
import scala.swing.event.{Event, ValueChanged}


class TtrMainFrame(control: Control) extends MainFrame {

  title = "Lambda TTR"

  preferredSize = new Dimension(640, 480)

  contents = new GridBagPanel {
    def constraints(x: Int, y: Int,
                    width: Int = 1, height: Int = 1,
                    xw: Double = 0.0, yw: Double = 0.0,
                    fill: GridBagPanel.Fill.Value = None,
                    anchor: GridBagPanel.Anchor.Value = Anchor.Center): Constraints = {
      val c = new Constraints
      c.gridx = x
      c.gridy = y
      c.gridwidth = width
      c.gridheight = height
      c.weightx = xw
      c.weighty = yw
      c.fill = fill
      c.anchor = anchor
      c
    }

    // trajectory

    add(new VerticalLabel("High") {
      border = aBorder
      xAlignment = Alignment.Right
    },
      constraints(x = 0, y = 1, width = 1, height = 3, xw = 0, yw = 0.33, fill = Vertical, anchor = South))

    add(new VerticalLabel("Trajectory") {
      border = aBorder
    }, constraints(x = 0, y = 3, width = 1, height = 3, xw = 0, yw=0, fill = Fill.Both))

    add(new VerticalLabel("Low") {
      border = aBorder
      xAlignment = Alignment.Left
    },
      constraints(x = 0, y = 6, width = 1, height = 3, xw = 0, yw = 0.33, fill = Vertical, anchor = South))

    add(new Slider() {
      name = "Trajectory control"
      border = aBorder
      min = -5
      max = 5
      value = 0
      majorTickSpacing = 1
      paintTicks = true
      paintLabels = true
      orientation = Orientation.Vertical
      reactions += toValueChanged(control.trajectory)
    },
      constraints(x = 1, y = 1, width = 1, height = 7, xw = 0, yw = 1, fill = Vertical)
    )

    // direction labels
    add(new Label("Left") {
      border = aBorder
      xAlignment = Alignment.Left
    }, constraints(x = 2, y = 0, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Label("Direction") {
      border = aBorder
      xAlignment = Alignment.Center
    }, constraints(x = 3, y = 0, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Label("Right") {
      border = aBorder
      xAlignment = Alignment.Right
    }, constraints(x = 4, y = 0, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))


    add(new Slider() {
      name = "Direction control"
      border = aBorder
      min = -5
      max = 5
      value = 0
      majorTickSpacing = 1
      paintTicks = true
      paintLabels = true
      reactions += toValueChanged(control.direction)
    }, constraints(x = 2, y = 1, width = 3, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = North))

    // speed
    add(new Label("Slow") {
      border = aBorder
      xAlignment = Alignment.Left
      yAlignment = Alignment.Bottom
    }, constraints(x = 2, y = 2, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Label("Speed") {
      border = aBorder
      xAlignment = Alignment.Center
    }, constraints(x = 3, y = 2, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Label("Fast") {
      border = aBorder
      xAlignment = Alignment.Right
      yAlignment = Alignment.Bottom
    }, constraints(x = 4, y = 2, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Slider() {
      name = "Speed control"
      border = aBorder
      min = 0
      max = 10
      value = 0
      majorTickSpacing = 1
      paintTicks = true
      paintLabels = true
      reactions += toValueChanged(control.speed)
    }, constraints(x = 2, y = 3, width = 3, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = North)
    )

    // back/top
    add(new Label("Backspin") {
      border = aBorder
      xAlignment = Alignment.Left
      yAlignment = Alignment.Bottom
    },
      constraints(x = 2, y = 4, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Label("Loop") {
      border = aBorder
      xAlignment = Alignment.Center
    }, constraints(x = 3, y = 4, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Label("Topspin") {
      border = aBorder
      xAlignment = Alignment.Right
    }, constraints(x = 4, y = 4, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Slider() {
      name = "Topspin control"
      border = aBorder
      min = -5
      max = 5
      value = 0
      majorTickSpacing = 1
      paintTicks = true
      paintLabels = true
      reactions += toValueChanged(control.topspinBackspin)
    }, constraints(x = 2, y = 5, width = 3, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = North))

    // left/right
    add(new Label("Left") {
      border = aBorder
      xAlignment = Alignment.Left
      yAlignment = Alignment.Bottom
    }, constraints(x = 2, y = 6, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Label("Swerve") {
      border = aBorder
      xAlignment = Alignment.Center
    }, constraints(x = 3, y = 6, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Label("Right") {
      border = aBorder
      xAlignment = Alignment.Right
      yAlignment = Alignment.Bottom
    }, constraints(x = 4, y = 6, width = 1, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = South))
    add(new Slider() {
      name = "Sidespin control"
      border = aBorder
      min = -5
      max = 5
      value = 0
      majorTickSpacing = 1
      paintTicks = true
      paintLabels = true
      reactions += toValueChanged(control.sidespin)
    }, constraints(x = 2, y = 7, width = 3, height = 1, xw = 1, yw = 0.125, fill = Horizontal, anchor = North)
    )
  }


  def toValueChanged(f: Int => Unit): PartialFunction[Event, Unit] = {
    case ValueChanged(slider: Slider) => f(slider.value)
  }

  def aBorder: Border = {
    new EmptyBorder(0,0,0,0)
  }
}

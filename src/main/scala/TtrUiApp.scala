import net.jtownson.ttr.comm.{MachineStateComms, SerialProtocol, StdioProtocol}
import net.jtownson.ttr.ui.{Control, TtrMainFrame}

object TtrUiApp {
  def main(args: Array[String]) {
    //val control = new Control(new MachineStateComms(new StdioProtocol))
    val control = new Control(new MachineStateComms(new SerialProtocol("cu.wchusbserial1410")))
    val ui = new TtrMainFrame(control)
    ui.visible = true
  }
}
